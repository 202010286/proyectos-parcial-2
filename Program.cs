﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MaquinaExpendedora
{
    class Program
    {
        static void Main(string[] args)
        {
            int opciones, entrada, x = 0;
            int monedas_5, monedas_10, monedas_25, suma = 0;
            do
            {
                Console.WriteLine("MAQUINA EXPENDEDORA");
                Console.WriteLine("1. Insertar las monedas");
                Console.WriteLine("2. Comprar");
                Console.WriteLine("3. Salir");
                Console.Write("Ingresar opcion: ");
                entrada = int.Parse(Console.ReadLine());
                if (entrada == 1)
                {
                    Console.Clear();
                    Console.Write("Insertar monedas de 5:");
                    monedas_5 = int.Parse(Console.ReadLine());
                    suma = suma + monedas_5 * 5;
                    Console.Write("Insertar monedas de 10:");
                    monedas_10 = int.Parse(Console.ReadLine());
                    suma = suma + monedas_10 * 10;
                    Console.Write("Insertar monedas de 25:");
                    monedas_25 = int.Parse(Console.ReadLine());
                    suma = suma + monedas_25 * 25;
                    Console.Clear();
                }

                if (entrada == 2)
                {
                    if (suma >= 10)
                    {
                        Console.Clear();
                        Console.WriteLine("Menú: ");
                        Console.WriteLine("1. Doritos");
                        Console.WriteLine("2. Papas fritas");
                        Console.WriteLine("3. Palitos de queso");
                        Console.WriteLine("4. Gatorade");
                        Console.WriteLine("5. Maní de limón");
                        Console.WriteLine("6. Fandango");
                        Console.WriteLine("7. Vive 100");
                        Console.WriteLine("8. Jugo de naranja");
                        Console.WriteLine("9. Galleta Oreo");
                        Console.WriteLine("10. M & M");
                        Console.Write("Ingresar opcion:");
                        opciones = int.Parse(Console.ReadLine());

                        switch (opciones)
                        {
                            case 1:
                                Console.Clear();
                                Console.WriteLine("Se dispenso un dorito.");
                                suma = suma - 25;
                                Console.WriteLine($"Su devuelta es de: {suma}");
                                x++;
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            case 2:
                                Console.Clear();
                                Console.WriteLine("Se dispenso una Papa frita.");
                                suma = suma - 15;
                                Console.WriteLine($"Su devuelta es de: {suma}");
                                x++;
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            case 3:
                                Console.Clear();
                                Console.WriteLine("Se dispenso un Palito de queso.");
                                suma = suma - 15;
                                Console.WriteLine($"Su devuelta es de: {suma}");
                                x++;
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            case 4:
                                Console.Clear();
                                Console.WriteLine("Se dispenso un Gatorade.");
                                suma = suma - 50;
                                Console.WriteLine($"Su devuelta es de: {suma}");
                                x++;
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            case 5:
                                Console.Clear();
                                Console.WriteLine("Se dispenso un Maní de limón.");
                                suma = suma - 10;
                                Console.WriteLine($"Su devuelta es de: {suma}");
                                x++;
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            case 6:
                                Console.Clear();
                                Console.WriteLine("Se dispenso un Fandango.");
                                suma = suma - 15;
                                Console.WriteLine($"Su devuelta es de: {suma}");
                                x++;
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            case 7:
                                Console.Clear();
                                Console.WriteLine("Se dispenso un Vive 100.");
                                suma = suma - 50;
                                Console.WriteLine($"Su devuelta es de: {suma}");
                                x++;
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            case 8:
                                Console.Clear();
                                Console.WriteLine("Se dispenso un Jugo de naranja.");
                                suma = suma - 25;
                                Console.WriteLine($"Su devuelta es de: {suma}");
                                x++;
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            case 9:
                                Console.Clear();
                                Console.WriteLine("Se dispenso una Galleta Oreo.");
                                suma = suma - 30;
                                Console.WriteLine($"Su devuelta es de: {suma}");
                                x++;
                                Console.ReadKey();
                                Console.Clear();
                                break;
                            case 10:
                                Console.Clear();
                                Console.WriteLine("Se dispenso un M & M.");
                                suma = suma - 5;
                                Console.WriteLine($"Su devuelta es de: {suma}");
                                x++;
                                Console.ReadKey();
                                Console.Clear();
                                break;
                        }

                    }
                    else
                    {
                        Console.WriteLine("Monto insuficiente.");
                        Console.WriteLine($"El monto insertado es: {suma}");
                    }
                }
                if (entrada == 3)
                {
                    Environment.Exit(0);
                }
            } while (entrada != 3);
        }
    }
}


